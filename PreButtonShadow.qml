import QtQuick 2.0

import QtQuick 2.0
import QtGraphicalEffects 1.12

Item
{
    id: btnRoot

    property int btnWidth: 0
    property int btnHeight: 0
    property int btnRadius: 0
    property int xPos: 0
    property int yPos: 0
    property int textSize: 5

    property color colorText: "white"
    property string buttonText: "Button"
    property color colorStandard: "#363636"
    property color colorHover: "#222831"
    property color colorPress: "slategray"

    property bool btnShadow: false

    scale: state === "Pressed" ? 1.0 : 1.0
    onEnabledChanged: state = ""

    signal clicked()
    signal pressed()
    signal released()

    Behavior on scale
    {
        NumberAnimation
        {
            duration: 100
            easing.type: Easing.InOutQuad
        }
    }

    Rectangle
    {
        id: btnRect

        width: btnRoot.btnWidth
        height: btnRoot.btnHeight
        radius: btnRoot.btnRadius
        x: btnRoot.xPos
        y: btnRoot.yPos
        color: btnRoot.colorStandard


        Text {
            id: btnText
            font.pixelSize: btnRoot.textSize
            anchors.centerIn: parent
            color: btnRoot.colorText
            text: btnRoot.buttonText
        }

        MouseArea
        {
            id: btnMouseArea
            anchors.fill: parent
            hoverEnabled: true

            onEntered:
            {
                btnRoot.state = "Hovering"
                //btnRect.color = btnRoot.colorHighlight
                btnRoot.pressed();
            }

            onExited: { btnRoot.state=''}

            onClicked: { btnRoot.clicked();}

            onPressed: { btnRoot.state='Pressed' }

            onReleased:
            {
                if (containsMouse)
                {
                    btnRoot.state='Hovering';
                }
                else
                {
                    btnRoot.state="";
                }

                btnRect.color = btnRoot.colorStandard
                btnRoot.released()
            }
        }
    }

    states:
        [
            State
            {
                name: "Hovering"
                PropertyChanges
                {
                    target: btnRect
                    color: colorHover
                }
            },

            State
            {
                name: "Pressed"
                PropertyChanges
                {
                    target: btnRect
                    color: colorPress
                }
            }
        ]

    transitions:
        [
            Transition
            {
                from: ""; to: "Hovering"
                ColorAnimation { duration: 200 }
            },

            Transition
            {
                from: "*"; to: "Pressed"
                ColorAnimation { duration: 10 }
            }
        ]

    DropShadow
    {
        anchors.fill: btnRect
        source: btnRect
        horizontalOffset: 2
        verticalOffset: 2
        radius: 5
        samples: 17
        color: "#80000000"
        enabled: btnRoot.btnShadow
    }
}

