import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12

import ScreenLink 1.0
import ScreenLink.Utils 1.0

Window
{
    id: root
    visible: true
    width: 900
    height: 500
    minimumHeight: height
    maximumHeight: height
    minimumWidth: width
    maximumWidth: width
    color: "#363636"
    title: qsTr("ScreenLink - ALPHA v0.3")

    PreButton
    {
        btnWidth: parent.width / 3
        btnHeight: 50
        textSize: 17
        buttonText: "Verbindung herstellen.."
        btnShadow: false
    }

    PreButton
    {
        btnWidth: parent.width / 3
        btnHeight: 50
        textSize: 17
        yPos: 50
        buttonText: "Kontakte"
        btnShadow: false
    }

    PreButton
    {
        btnWidth: parent.width / 3
        btnHeight: 50
        textSize: 17
        yPos: 100
        buttonText: "Optionen"

        onPressed:
        {

        }
    }

    Image
    {
        id:labelImage
        source: "src/sl_label.png"
        x: parent.width / 12
        y: 370
        height: 120
        width: 150
    }

    Rectangle
    {
        id: verticalSeperator
        x: parent.width / 3
        width: 2
        height: parent.height
        opacity: .4
        color: "#000000"
    }


    Label
    {
        id: idLabel
        text: "ID:"
        color: "white"
        font.pixelSize: 20
        width: parent.width / 3
        height: 40
        x: 310
        y: 12
    }

    Label
    {
        id: pswdLabel
        text: "Passwort:"
        color: "white"
        font.pixelSize: 20
        width: parent.width / 3
        height: 40
        x: 310
        y: 47
    }

    Rectangle
    {
        id: verticalSeperator2
        x: ((parent.width + parent.width / 3) / 2) + 2
        width: 2
        height: parent.height
        opacity: .4
        color: "#000000"
    }

    TextField
    {
        id: idTextField
        placeholderText: "Bitte gebe eine ID ein.."
        x:((parent.width + parent.width / 3) / 2) + 4
        y: 7
        // Only allow INT
        validator: IntValidator {bottom: 1}
        maximumLength: 8
        font.pixelSize: 20
        color: "white"
        background: Rectangle {
        radius: 2
        implicitWidth: 300
        implicitHeight: 24
        border.color: "#333"
        border.width: 1
        color: "#363636"
        }
    }

    PreButtonShadow
    {
        id: btnID
        btnWidth: 140
        btnHeight: 35
        btnRadius: 6
        colorStandard: "#005082"
        textSize: 15
        xPos: parent.width / 6 * 5
        yPos: 60
        buttonText: "Verbinden!"
    }

    Rectangle
    {
        id: horizontalSeperator
        x: ((parent.width + parent.width / 3) / 2) + 4
        y: 450
        width: 300
        height: 2
        opacity: .4
        color: "#000000"
    }

    Label
    {
        id: statusLabel
        text: "Status: Nicht verbunden.."
        color: "white"
        font.pixelSize: 14
        width: parent.width / 3
        height: 40
        x: ((parent.width + parent.width / 3) / 2) + 15
        y: 467
    }
}
