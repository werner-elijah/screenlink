#ifndef MOUSEMANAGER_H
#define MOUSEMANAGER_H

#include <QRect>
#include <QPoint>
#include <QCursor>
#include <QScreen>
#include <QGuiApplication>

class mouseManager
{

public:
    mouseManager();
    QPoint getNativeCursorPosition();
};

#endif // MOUSEMANAGER_H
