#include "mousemanager.h"
#include <QDebug>
mouseManager::mouseManager(){}

QPoint getNativeCursorPosition()
{
    QPoint pos = getNativeCursorPosition();

    // Cursor positions from Qt are calculated in a strange way, the offset to
    // the origin of the current screen is in device-independent pixels while
    // the origin itself is native!

    for (QScreen *screen : QGuiApplication::screens()) {
        QRect screenRect = screen->geometry();
        if (screenRect.contains(pos)) {
            QPoint origin = screenRect.topLeft();
            return origin + (pos - origin) * screen->devicePixelRatio();
        }
    }

    // should not happen, but try to find a good fallback.
    return pos * qApp->devicePixelRatio();
}
