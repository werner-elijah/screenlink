#ifndef WINDOWMANAGER_H
#define WINDOWMANAGER_H
#include <QString>
#include <QObject>
#include <QVariant>

class windowmanager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString test READ test WRITE setTest NOTIFY testChanged)

signals:
    void testChanged();

public:
    explicit windowmanager(QObject *parent = nullptr);
    QString test();
    void setTest(QString v);
};

#endif // WINDOWMANAGER_H
