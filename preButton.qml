import QtQuick 2.0
import QtGraphicalEffects 1.12

Item
{
    id: btnRoot

    property int btnWidth: 0
    property int btnHeight: 0
    property int btnRadius: 0
    property int xPos: 0
    property int yPos: 0
    property int textSize: 5

    property string colorText: "white"
    property string buttonText: "Button"
    property string colorStandard: "#363636"
    property string colorHighlight: "#222831"

    property bool btnShadow: false

    signal clicked()
    signal pressed()
    signal released()

    Rectangle
    {
        id: btnRect

        width: btnRoot.btnWidth
        height: btnRoot.btnHeight
        radius: btnRoot.btnRadius
        x: btnRoot.xPos
        y: btnRoot.yPos
        color: btnRoot.colorStandard

        Text {
            id: btnText
            font.pixelSize: btnRoot.textSize
            anchors.centerIn: parent
            color: btnRoot.colorText
            text: btnRoot.buttonText
        }

        MouseArea
        {
            id: btnMouseArea
            anchors.fill: parent

            onEntered:
            {
                btnRect.color = btnRoot.colorHighlight
                btnRoot.pressed();
            }

            onReleased:
            {
                btnRect.color = btnRoot.colorStandard
                btnRoot.released()
                btnRoot.clicked()
            }
        }
    }

    /*DropShadow
    {
        anchors.fill: btnRect
        source: btnRect
        horizontalOffset: 2
        verticalOffset: 2
        radius: 5
        samples: 17
        color: "#80000000"
        enabled: btnRoot.btnShadow
    }*/
}
