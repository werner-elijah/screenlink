#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "windowmanager.h"
#include "connectionmanager.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    qmlRegisterType<windowmanager>("ScreenLink", 1,0, "WindowManager");
    qmlRegisterType<connectionManager>("ScreenLink.Utils", 1,0, "ConnectionManager");

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
