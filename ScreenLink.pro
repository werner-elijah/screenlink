TEMPLATE = app

QT += core \
    quick \
    qml \
    network

CONFIG += c++17

RC_ICONS = $$PWD/src/slicon.ico

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        $$PWD/main.cpp \
        connectionmanager.cpp \
        mousemanager.cpp \
        windowmanager.cpp

RESOURCES += $$PWD/qml.qrc \
    $$PWD/presets.qrc \
    images.qrc

INCLUDEPATH += \
    $$PWD/src/\

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    connectionmanager.h \
    mousemanager.h \
    windowmanager.h

DISTFILES += \
    src/sl_label.png \
    src/slicon.ico
